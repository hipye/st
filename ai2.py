from openai import OpenAI
from rich.console import Console
from rich.markdown import Markdown
from rich.live import Live
import os
import tempfile
import subprocess
from prompt_toolkit import prompt
from prompt_toolkit.key_binding import KeyBindings
from prompt_toolkit.keys import Keys
import pyperclip

# 初始化 OpenAI 客户端
api_key = os.getenv("DEEPSEEKAPI")
client = OpenAI(api_key=api_key, base_url="https://api.deepseek.com")

# 初始化 Rich 控制台
console = Console()

# 渲染 Markdown 内容，不使用 Panel
def render_markdown(content):
    return Markdown(content)

# 获取 API 响应
def get_api_response(client, messages, deep_think=False):
    return client.chat.completions.create(
        model="deepseek-chat",  # 使用 DeepSeek 的模型
        messages=messages,
        max_tokens=500 if deep_think else 300,  # 深度思考时增加响应长度
        temperature=0.7 if deep_think else 0.9,  # 深度思考时降低随机性
        top_p=0.9 if deep_think else 1.0,  # 深度思考时增加多样性
        stream=True
    )

# 渲染流式响应
def render_response(response):
    full_response = ""
    with Live(auto_refresh=False) as live:
        for chunk in response:
            if chunk.choices[0].delta.content:
                full_response += chunk.choices[0].delta.content
                live.update(render_markdown(full_response), refresh=True)
    return full_response

# 将回答写入文本文件
def write_to_file(content, filename="output.txt"):
    try:
        with open(filename, "a", encoding="utf-8") as file:
            file.write(content + "\n\n")
        console.print(f"[green]回答已写入文件 {filename}。[/green]")
    except Exception as e:
        console.print(f"[red]写入文件时发生错误：{e}[/red]")

# 使用 vim 获取用户输入，默认启用 :set paste
def get_input_with_vim():
    # 创建一个临时文件
    with tempfile.NamedTemporaryFile(suffix=".txt", delete=False) as tmp_file:
        tmp_filename = tmp_file.name

    # 调用 vim 编辑临时文件，默认启用 :set paste
    try:
        subprocess.run(["vim", "-c", "set paste", tmp_filename], check=True)
    except subprocess.CalledProcessError:
        console.print("[red]调用 vim 时发生错误。[/red]")
        return ""

    # 读取临时文件内容
    with open(tmp_filename, "r", encoding="utf-8") as file:
        content = file.read().strip()

    # 删除临时文件
    os.remove(tmp_filename)

    return content

# 获取多行用户输入，支持回车发送、Shift+回车换行、Ctrl+Q 深度思考、Ctrl+W 写入文件、Ctrl+L 切换到 vim
def get_multiline_input():
    console.print("[bold cyan]请输入你的内容（按回车发送，Shift+回车换行，Ctrl+Q 深度思考，Ctrl+W 写入文件，Ctrl+L 切换到 vim）：[/bold cyan]")
    bindings = KeyBindings()

    # 绑定回车发送
    @bindings.add(Keys.Enter)
    def _(event):
        if event.current_buffer.document.is_cursor_at_the_end:  # 如果光标在末尾，则发送
            event.app.exit(result=event.current_buffer.text)
        else:  # 否则插入换行符
            event.current_buffer.insert_text("\n")

    # 绑定 Ctrl+Q 深度思考
    @bindings.add("c-q")  # 绑定 Ctrl+Q
    def _(event):
        event.app.exit(result="[DEEP_THINK]")  # 返回特殊标记，表示启用深度思考

    # 绑定 Ctrl+W 写入文件
    @bindings.add("c-w")  # 绑定 Ctrl+W
    def _(event):
        event.app.exit(result="[WRITE_TO_FILE]")  # 返回特殊标记，表示写入文件

    # 绑定 Ctrl+L 切换到 vim
    @bindings.add("c-l")  # 绑定 Ctrl+L
    def _(event):
        event.app.exit(result="[VIM_EDIT]")  # 返回特殊标记，表示切换到 vim

    # 绑定 Esc+v 粘贴
    @bindings.add(Keys.Escape, "v")
    def _(event):
        clipboard_text = pyperclip.paste()  # 获取剪贴板内容
        if clipboard_text:
            # 替换换行符为空格，确保一次性粘贴
            clipboard_text = clipboard_text.replace("\n", " ").replace("\r", " ")
            event.current_buffer.insert_text(clipboard_text)  # 插入剪贴板内容
            console.print("[green]已粘贴剪贴板内容。[/green]")

    try:
        user_input = prompt("", key_bindings=bindings, multiline=True)
        return user_input
    except KeyboardInterrupt:  # 处理 Ctrl+C
        return ""
    except EOFError:  # 处理 Ctrl+D
        return "[DEEP_THINK]"

# 实时问答循环
def real_time_chat():
    console.print("实时问答已启动！输入 'exit' 退出。", style="bold green")

    # 初始化对话上下文
    messages = [
        {"role": "system", "content": "你是一个乐于助人的助手。请始终以 Markdown 格式返回你的回答。"}
    ]

    while True:
        # 获取用户输入
        user_input = get_multiline_input().strip()
        if not user_input:
            console.print("[yellow]输入不能为空，请重新输入。[/yellow]")
            continue
        if user_input.lower() == 'exit':  # 检查用户输入是否为 'exit'
            console.print("退出实时问答。", style="bold red")
            break

        # 检查是否启用深度思考
        deep_think = user_input == "[DEEP_THINK]"
        if deep_think:
            user_input = "请对上一个问题进行深度思考，并提供详细回答。"

        # 检查是否写入文件
        write_to_file_flag = user_input == "[WRITE_TO_FILE]"
        if write_to_file_flag:
            if messages and messages[-1]["role"] == "assistant":
                write_to_file(messages[-1]["content"])
            else:
                console.print("[yellow]没有可写入的回答。[/yellow]")
            continue

        # 检查是否切换到 vim
        if user_input == "[VIM_EDIT]":
            user_input = get_input_with_vim().strip()
            if not user_input:
                console.print("[yellow]输入不能为空，请重新输入。[/yellow]")
                continue

        # 将用户输入添加到对话上下文中
        messages.append({"role": "user", "content": user_input})

        # 渲染用户输入的 Markdown 内容
        console.print(render_markdown(user_input))

        try:
            # 调用 DeepSeek API，启用流式响应
            response = get_api_response(client, messages, deep_think=deep_think)

            # 渲染流式响应
            full_response = render_response(response)

            # 将API回答添加到对话上下文中
            messages.append({"role": "assistant", "content": full_response})
        except Exception as e:
            console.print(f"[red]请求发生异常：{e}[/red]")

# 启动实时问答
if __name__ == "__main__":
    real_time_chat()
