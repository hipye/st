# 进入 OpenWrt 源码目录
cd ~/lede

# 创建构建目录
mkdir -p build_dir/target-arm_cortex-a7+neon-vfpv4_musl_eabi/luci-app-r1-server-1.0

# 复制 Makefile
cp package/feeds/luci/applications/luci-app-r1-server/Makefile \
   build_dir/target-arm_cortex-a7+neon-vfpv4_musl_eabi/luci-app-r1-server-1.0/

# 复制 LuCI 控制器文件
mkdir -p build_dir/target-arm_cortex-a7+neon-vfpv4_musl_eabi/luci-app-r1-server-1.0/luasrc/controller
cp package/feeds/luci/applications/luci-app-r1-server/luasrc/controller/r1-server.lua \
   build_dir/target-arm_cortex-a7+neon-vfpv4_musl_eabi/luci-app-r1-server-1.0/luasrc/controller/

# 复制 LuCI 视图文件
mkdir -p build_dir/target-arm_cortex-a7+neon-vfpv4_musl_eabi/luci-app-r1-server-1.0/luasrc/view
cp package/feeds/luci/applications/luci-app-r1-server/luasrc/view/r1-server.htm \
   build_dir/target-arm_cortex-a7+neon-vfpv4_musl_eabi/luci-app-r1-server-1.0/luasrc/view/

# 复制 Go 程序
mkdir -p build_dir/target-arm_cortex-a7+neon-vfpv4_musl_eabi/luci-app-r1-server-1.0/root/usr/bin
cp package/feeds/luci/applications/luci-app-r1-server/root/usr/bin/r1-server \
   build_dir/target-arm_cortex-a7+neon-vfpv4_musl_eabi/luci-app-r1-server-1.0/root/usr/bin/

# 复制 Shell 脚本
cp package/feeds/luci/applications/luci-app-r1-server/root/usr/bin/r1-server-control.sh \
   build_dir/target-arm_cortex-a7+neon-vfpv4_musl_eabi/luci-app-r1-server-1.0/root/usr/bin/

# 设置执行权限
chmod +x build_dir/target-arm_cortex-a7+neon-vfpv4_musl_eabi/luci-app-r1-server-1.0/root/usr/bin/r1-server
chmod +x build_dir/target-arm_cortex-a7+neon-vfpv4_musl_eabi/luci-app-r1-server-1.0/root/usr/bin/r1-server-control.sh
