#/bin/bash


EXPAT=https://github.com/libexpat/libexpat/releases/download/R_2_5_0/expat-2.5.0.tar.bz2
OPENSSL=https://www.openssl.org/source/openssl-1.1.1w.tar.gz
C_ARES=https://c-ares.org/download/c-ares-1.19.1.tar.gz
SSH2=https://libssh2.org/download/libssh2-1.11.0.tar.gz
ZLIB=https://github.com/madler/zlib/releases/download/v1.3/zlib-1.3.tar.gz
ARIA2=https://github.com/aria2/aria2/releases/download/release-1.36.0/aria2-1.36.0.tar.bz2
SQLITE3=https://sqlite.org/2023/sqlite-autoconf-3430100.tar.gz

DOWNLOADER="wget"
TOOLCHAIN=/usr/bin/
PATH=/usr/bin:$PATH
HOST=aarch64-linux-gnu
PREFIX=/inspack/usr/local
LOCAL_DIR=/inspack/usr/local
TOOL_BIN_DIR=/usr/bin
PATH=${TOOL_BIN_DIR}:$PATH
CFLAGS="-march=armv8-a -mtune=cortex-a53"
DEST=/inspack/usr/local
CC=$HOST-gcc
CXX=$HOST-g++
LDFLAGS="-L$DEST/lib"
CPPFLAGS="-I$DEST/include"
CXXFLAGS=$CFLAGS


function zlib_i() {
cd /tmp
 # zlib library build
  $DOWNLOADER $ZLIB
  tar zxvf zlib-1.3.tar.gz
  cd zlib-1.3/
  PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig/ LD_LIBRARY_PATH=$PREFIX/lib/ CC=$HOST-gcc STRIP=$HOST-strip RANLIB=$HOST-ranlib CXX=$HOST-g++ AR=$HOST-ar LD=$HOST-ld ./configure --prefix=$PREFIX --static
  make
  make install
}
function expat_i() {
# expat library build
  cd /tmp
  $DOWNLOADER $EXPAT
  tar xf expat-2.5.0.tar.bz2
  cd expat-2.5.0/
  PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig/ LD_LIBRARY_PATH=$PREFIX/lib/ CC=$HOST-gcc CXX=$HOST-g++ ./configure --host=$HOST --build=`dpkg-architecture -qDEB_BUILD_GNU_TYPE` --prefix=$PREFIX --enable-static=yes --enable-shared=no
  make
  make install
}
function c_ares_i() {
  cd /tmp
  $DOWNLOADER $C_ARES
  tar xf c-ares-1.19.1.tar.gz
  cd c-ares-1.19.1/
  PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig/ LD_LIBRARY_PATH=$PREFIX/lib/ CC=$HOST-gcc CXX=$HOST-g++ ./configure --host=$HOST --build=`dpkg-architecture -qDEB_BUILD_GNU_TYPE` --prefix=$PREFIX --enable-static --disable-shared
  make
  make install
}
function openssl_i() {
cd /tmp
  $DOWNLOADER $OPENSSL
  tar xf openssl-1.1.1w.tar.gz
  cd openssl-1.1.1w/
  PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig/ LD_LIBRARY_PATH=$PREFIX/lib/ CC=$HOST-gcc CXX=$HOST-g++ ./Configure linux-aarch64 $CFLAGS --prefix=$PREFIX shared zlib zlib-dynamic -D_GNU_SOURCE -D_BSD_SOURCE --with-zlib-lib=$LOCAL_DIR/lib --with-zlib-include=$LOCAL_DIR/include
  make
  make install
}
function ssh2_i() {
cd /tmp
  $DOWNLOADER $SSH2
  tar xf libssh2-1.11.0.tar.gz
  cd libssh2-1.11.0/
  rm -rf $PREFIX/lib/pkgconfig/libssh2.pc
  PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig/ LD_LIBRARY_PATH=$PREFIX/lib/ CC=$HOST-gcc CXX=$HOST-g++ AR=$HOST-ar RANLIB=$HOST-ranlib ./configure --host=$HOST --without-libgcrypt --with-openssl --without-wincng --prefix=$PREFIX --enable-static --disable-shared
  make
  make install
}
function sqlite_i() {
  cd /tmp
  $DOWNLOADER $SQLITE3
  tar zxvf sqlite-autoconf-3430100.tar.gz
  cd sqlite-autoconf-3430100/
  PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig/ LD_LIBRARY_PATH=$PREFIX/lib/ CC="$C_COMPILER" CXX="$CXX_COMPILER" ./configure --prefix=$PREFIX --enable-static --enable-shared
  make
  make install
}
function aria2_i() {
cd /tmp
$DOWNLOADER $ARIA2
rr aria2-1.36.0
tar xf aria2-1.36.0.tar.bz2
cd aria2-1.36.0
#sed -i 's|-lrt||g;s|-lpthread||g' $(grep -rEl '\-lrt|\-lpthread' .)
#sed -i 's|timegm(|timegm_(|g' $(grep -rl 'timegm(' .)
sed -i 's/"1", 1, 16/"128", 1, -1/g' ./src/OptionHandlerFactory.cc
sed -i 's/"20M", 1_m, 1_g/"4K", 1_k, 1_g/g' ./src/OptionHandlerFactory.cc
sed -i 's/PREF_CONNECT_TIMEOUT, TEXT_CONNECT_TIMEOUT, "60", 1, 600/PREF_CONNECT_TIMEOUT, TEXT_CONNECT_TIMEOUT, "30", 1, 600/g' ./src/OptionHandlerFactory.cc
sed -i 's/PREF_PIECE_LENGTH, TEXT_PIECE_LENGTH, "1M", 1_m, 1_g/PREF_PIECE_LENGTH, TEXT_PIECE_LENGTH, "4k", 1_k, 1_g/g' ./src/OptionHandlerFactory.cc
sed -i 's/new NumberOptionHandler(PREF_RETRY_WAIT, TEXT_RETRY_WAIT, "0", 0, 600/new NumberOptionHandler(PREF_RETRY_WAIT, TEXT_RETRY_WAIT, "2", 0, 600/g' ./src/OptionHandlerFactory.cc
sed -i 's/new NumberOptionHandler(PREF_SPLIT, TEXT_SPLIT, "5", 1, -1,/new NumberOptionHandler(PREF_SPLIT, TEXT_SPLIT, "8", 1, -1,/g' ./src/OptionHandlerFactory.cc
PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig/ \
LD_LIBRARY_PATH=$PREFIX/lib/ \
CXX=$HOST-g++ \
CC=$HOST-gcc \
AR=$HOST-ar \
RANLIB=$HOST-ranlib \
./configure \
--host=$HOST \
--build=`dpkg-architecture -qDEB_BUILD_GNU_TYPE` \
--prefix=$PREFIX/ \
--disable-nls \
--without-gnutls \
--with-openssl \
--without-libxml2 \
--with-libz --with-libz-prefix=$PREFIX/ \
--with-libexpat --with-libexpat-prefix=$PREFIX/ \
--with-slite3 --with-sqlite3-prefix=$PREFIX/ \
--with-libcares --with-libcares-prefix=$PREFIX/ \
--with-ca-bundle='/etc/ssl/certs/ca-certificates.crt' \
LDFLAGS="-L$PREFIX/lib" \
PKG_CONFIG_PATH="$PREFIX/lib/pkgconfig" \
ARIA2_STATIC=yes
make -j4
}
proxy_on
#zlib_i && expat_i && c_ares_i && openssl_i && ssh2_i && sqlite_i
aria2_i
