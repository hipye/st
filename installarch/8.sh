pacman -S grub efibootmgr networkmanager network-manager-applet dialog wireless_tools wpa_supplicant os-prober mtools dosfstools ntfs-3g base-devel linux-headers reflector git sudo intel-ucode deepin deepin-extra lightdm lightdm-deepin-greeter
echo "ok"
sed -i '5a GRUB_DISABLE_OS_PROBER=false' /etc/default/grub
echo ">>>"
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=Arch
echo "==="
grub-mkconfig -o /boot/grub/grub.cfg
