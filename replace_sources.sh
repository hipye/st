#!/bin/bash

# OpenWrt/LEDE 替换下载链接为清华源和 GitHub 加速源，并修改 feeds.conf.default
# 作者：你的名字
# 版本：1.3
# 描述：自动替换下载链接为清华源和 gh.llkk.cc 加速源，并修改 feeds.conf.default 为清华源。

# 配置
MIRROR_URL="https://mirrors.tuna.tsinghua.edu.cn"  # 清华源地址
GITHUB_MIRROR="https://gh.llkk.cc"  # GitHub 加速源地址
WORK_DIR="/root/lede"  # OpenWrt/LEDE 源码目录

# 进入工作目录
cd "$WORK_DIR" || { echo "目录 $WORK_DIR 不存在！"; exit 1; }

# 替换下载链接为清华源
echo "正在替换下载链接为清华源..."
find . -type f -name "Makefile" -exec sed -i \
    -e "s|http://downloads.openwrt.org|$MIRROR_URL/openwrt|g" \
    -e "s|https://downloads.openwrt.org|$MIRROR_URL/openwrt|g" \
    -e "s|http://roy.marples.name|$MIRROR_URL|g" \
    -e "s|https://roy.marples.name|$MIRROR_URL|g" \
    {} +

# 替换 GitHub 链接为 gh.llkk.cc 加速源
echo "正在替换 GitHub 链接为 gh.llkk.cc 加速源..."
find . -type f -name "Makefile" -exec sed -i \
    -e "s|https://github.com|$GITHUB_MIRROR/https://github.com|g" \
    -e "s|http://github.com|$GITHUB_MIRROR/https://github.com|g" \
    {} +

# 修改 feeds.conf.default 文件为清华源
FEEDS_CONF="feeds.conf.default"
if [ -f "$FEEDS_CONF" ]; then
    echo "正在修改 $FEEDS_CONF 文件为清华源..."
    sed -i \
        -e "s|https://github.com/openwrt/packages|$MIRROR_URL/git/openwrt/packages.git|g" \
        -e "s|https://github.com/openwrt/luci|$MIRROR_URL/git/openwrt/luci.git|g" \
        -e "s|https://github.com/openwrt/routing|$MIRROR_URL/git/openwrt/routing.git|g" \
        -e "s|https://github.com/openwrt/telephony|$MIRROR_URL/git/openwrt/telephony.git|g" \
        "$FEEDS_CONF"
    echo "$FEEDS_CONF 文件修改完成！"
else
    echo "未找到 $FEEDS_CONF 文件，跳过修改。"
fi

echo "所有链接替换和修改完成！"
