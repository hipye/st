#!/bin/bash

# 默认值
IP="192.168.8.1"
SSID="LEDE"
PASSWORD="12345678a"
COUNTRY="CN"
MIRROR_URL="https://mirrors.aliyun.com/openwrt/releases/23.05.5"
OPENWRT_DIR="$HOME/lede"  # OpenWrt 源码目录
CREATE_UNBOUND=false  # 默认不创建 unbound 配置文件

# 解析命令行参数
while getopts "i:s:p:c:m:u" opt; do
    case $opt in
        i) IP="$OPTARG" ;;
        s) SSID="$OPTARG" ;;
        p) PASSWORD="$OPTARG" ;;
        c) COUNTRY="$OPTARG" ;;
        m) MIRROR_URL="$OPTARG" ;;
        u) CREATE_UNBOUND=true ;;
        *) echo "用法: $0 [-i IP] [-s SSID] [-p PASSWORD] [-c COUNTRY] [-m MIRROR_URL] [-u]" ;;
    esac
done

# 日志函数
log() {
    echo "[$(date '+%Y-%m-%d %H:%M:%S')] $1"
}

# 备份文件
backup_file() {
    local file="$1"
    if [ -f "$file" ]; then
        cp "$file" "${file}.bak"
        log "已备份 $file 为 ${file}.bak"
    else
        log "错误：未找到 $file 文件，无法备份"
        exit 1
    fi
}

# 生成新的 wireless 配置文件
WIRELESS_CONFIG_FILE="$OPENWRT_DIR/package/base-files/files/etc/config/wireless"
if [ -f "$WIRELESS_CONFIG_FILE" ]; then
    backup_file "$WIRELESS_CONFIG_FILE"
    cat << EOF > "$WIRELESS_CONFIG_FILE"
config wifi-device 'radio0'
    option type 'mac80211'
    option path 'platform/soc/a000000.wifi'
    option channel '1'
    option band '2g'
    option htmode 'HT20'
    option country '$COUNTRY'

config wifi-iface 'default_radio0'
    option device 'radio0'
    option network 'lan'
    option mode 'ap'
    option ssid '$SSID'
    option encryption 'psk2'
    option key '$PASSWORD'

config wifi-device 'radio1'
    option type 'mac80211'
    option path 'platform/soc/a800000.wifi'
    option htmode 'VHT80'
    option country '$COUNTRY'
    option cell_density '0'

config wifi-iface 'wifinet1'
    option device 'radio1'
    option mode 'sta'
    option network 'wwan'
    option ssid '8888'
    option encryption 'psk2'
    option key '33338888'
EOF
    log "已将无线配置信息添加到 $WIRELESS_CONFIG_FILE"
else
    log "未找到 $WIRELESS_CONFIG_FILE 文件，正在创建新文件"
    mkdir -p "$(dirname "$WIRELESS_CONFIG_FILE")"
    cat << EOF > "$WIRELESS_CONFIG_FILE"
config wifi-device 'radio0'
    option type 'mac80211'
    option path 'platform/soc/a000000.wifi'
    option channel '1'
    option band '2g'
    option htmode 'HT20'
    option country '$COUNTRY'

config wifi-iface 'default_radio0'
    option device 'radio0'
    option network 'lan'
    option mode 'ap'
    option ssid '$SSID'
    option encryption 'psk2'
    option key '$PASSWORD'

config wifi-device 'radio1'
    option type 'mac80211'
    option path 'platform/soc/a800000.wifi'
    option htmode 'VHT80'
    option country '$COUNTRY'
    option cell_density '0'

config wifi-iface 'wifinet1'
    option device 'radio1'
    option mode 'sta'
    option network 'wwan'
    option ssid '8888'
    option encryption 'psk2'
    option key '33338888'
EOF
    log "已创建新的 $WIRELESS_CONFIG_FILE 并添加无线配置信息"
fi

# 修改软件源
REPOSITORIES_CONF_FILE="$OPENWRT_DIR/repositories.conf"
log "正在配置软件源为 $MIRROR_URL"

# 创建或覆盖 repositories.conf 文件
cat <<EOF > "$REPOSITORIES_CONF_FILE"
src/gz openwrt_core $MIRROR_URL/targets/ipq40xx/generic/packages
src/gz openwrt_base $MIRROR_URL/packages/arm_cortex-a7_neon-vfpv4/base
src/gz openwrt_luci $MIRROR_URL/packages/arm_cortex-a7_neon-vfpv4/luci
src/gz openwrt_packages $MIRROR_URL/packages/arm_cortex-a7_neon-vfpv4/packages
src/gz openwrt_routing $MIRROR_URL/packages/arm_cortex-a7_neon-vfpv4/routing
src/gz openwrt_telephony $MIRROR_URL/packages/arm_cortex-a7_neon-vfpv4/telephony
EOF

# 将 repositories.conf 文件复制到固件文件系统中
mkdir -p "$OPENWRT_DIR/files/etc/opkg"
cp "$REPOSITORIES_CONF_FILE" "$OPENWRT_DIR/files/etc/opkg/repositories.conf"
log "已将软件源配置写入固件文件系统"

# 将默认 Shell 修改为 bash
log "正在将默认 Shell 修改为 bash"

# 1. 确保 bash 已包含在固件中
log "确保 bash 已包含在固件中"
echo "CONFIG_PACKAGE_bash=y" >> "$OPENWRT_DIR/.config"

# 2. 修改 /etc/passwd 文件，将 root 的 Shell 修改为 bash
log "修改 /etc/passwd 文件"
PASSWD_FILE="$OPENWRT_DIR/package/base-files/files/etc/passwd"  # 修正路径
mkdir -p "$(dirname "$PASSWD_FILE")"
if [ -f "$PASSWD_FILE" ]; then
    backup_file "$PASSWD_FILE"
    # 生成新的 passwd 文件内容
    cat << EOF > "$PASSWD_FILE"
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/bin/false
EOF
    log "已将 root 的默认 Shell 修改为 bash"
else
    log "错误：未找到 $PASSWD_FILE 文件"
    exit 1
fi

# 添加 SSH 公钥到 package/base-files/files/etc/dropbear/authorized_keys
SSH_KEY_FILE=""
if [ -f "$OPENWRT_DIR/id_rsa.pub" ]; then
    SSH_KEY_FILE="$OPENWRT_DIR/id_rsa.pub"
elif [ -f "$OPENWRT_DIR/id_ed25519.pub" ]; then
    SSH_KEY_FILE="$OPENWRT_DIR/id_ed25519.pub"
fi

if [ -n "$SSH_KEY_FILE" ]; then
    log "检测到 SSH 公钥文件 $SSH_KEY_FILE，正在添加到固件的 package/base-files/files/etc/dropbear/authorized_keys 文件中"
    AUTH_KEYS_FILE="$OPENWRT_DIR/package/base-files/files/etc/dropbear/authorized_keys"
    mkdir -p "$(dirname "$AUTH_KEYS_FILE")"
    mkdir -p package/base-files/files/etc/dropbear/
    touch package/base-files/files/etc/dropbear/authorized_keys
    backup_file "$AUTH_KEYS_FILE"
    cat "$SSH_KEY_FILE" > "$AUTH_KEYS_FILE"
    chmod 600 "$AUTH_KEYS_FILE"
    log "已将 SSH 公钥添加到固件的 package/base-files/files/etc/dropbear/authorized_keys 文件中"
else
    log "未检测到 SSH 公钥文件（id_rsa.pub 或 id_ed25519.pub），跳过 SSH 密钥添加"
fi

BASHRC_FILE="$OPENWRT_DIR/package/base-files/files/root/.bashrc"
mkdir -p package/base-files/files/root
touch package/base-files/files/root/.bashrc
if [ -f "$BASHRC_FILE" ]; then
backup_file "$BASHRC_FILE"
cat <<EOF > "$BASHRC_FILE"
# If not running interactively, don't do anything
[[ -z "\$PS1" ]] && return

# Check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# Set variable identifying the chroot you work in (used in the prompt below)
if [[ -z "\${debian_chroot:-}" ]] && [[ -r /etc/debian_chroot ]]; then
    debian_chroot=\$(cat /etc/debian_chroot)
fi

# Set a fancy prompt (non-color, overwrite the one in /etc/profile)
# but only if not SUDOing and have SUDO_PS1 set; then assume smart user.
if ! [[ -n "\${SUDO_USER}" && -n "\${SUDO_PS1}" ]]; then
    PS1="[\[\e[1;31m\]\u\[\e[m\]\[\e[1;37m\]@\A:\[\e[m\]\[\e[1;33m\]\w\[\e[m\]]\\$ "
fi
EOF
    log "已将自定义 bashrc 文件添加到固件中"
else
    log "错误：未找到 $BASHRC_FILE 文件"
    exit 1
fi

# 添加自定义 vimrc 文件
log "正在添加自定义 vimrc 文件"
VIMRC_FILE="$OPENWRT_DIR/package/base-files/files/root/.vimrc"
mkdir -p package/base-files/files/root
touch package/base-files/files/root/.vimrc
if [ -f "$VIMRC_FILE" ]; then
    backup_file "$VIMRC_FILE"
    cat <<EOF > "$VIMRC_FILE"
" 关闭 vi 兼容模式
set nocompatible

" 始终显示状态栏
set laststatus=2
" 设置 Tab 缩进为 4 个空格
set tabstop=4     " 一个 Tab 显示为 4 个空格
set shiftwidth=4  " 自动缩进时使用的空格数
set softtabstop=4 " 编辑时按 Tab 插入的空格数
set expandtab     " 将 Tab 转换为空格（保持代码跨环境一致性）
" 自定义状态栏
set statusline=
set statusline+=%<%F\                " 文件路径
set statusline+=%h%m%r%w\            " 帮助文件标志、修改标志、只读标志、预览
set statusline+=[%{&ff}]\            " 文件格式（unix/dos/mac）
set statusline+=[%Y]\                " 文件类型
set statusline+=%#warningmsg#        " 切换到警告颜色
set statusline+=%*                   " 恢复默认颜色
set statusline+=%=                   " 切换到右侧
set statusline+=%-14.(%l,%c%V%)\     " 行号、列号、虚拟列号
set statusline+=%P\                  " 文件位置百分比
" 在插入模式下按 Ctrl+x 保存并退出
inoremap <C-x> <Esc>:wq<CR>
" 在普通模式下按 Ctrl+x 保存并退出
nnoremap <C-x> :wq<CR>
nnoremap <C-i> :set paste<CR>
inoremap <C-q> <Esc>:q!<CR>
nnoremap <C-q> :q!<CR>
EOF
    log "已将自定义 vimrc 文件添加到固件中"
else
    log "错误：未找到 $VIMRC_FILE 文件"
    exit 1
fi

# 如果指定了创建 unbound 配置文件
if $CREATE_UNBOUND; then
    UNBOUND_CONFIG_FILE="$OPENWRT_DIR/package/base-files/files/etc/config/unbound"
    log "正在创建 unbound 配置文件 $UNBOUND_CONFIG_FILE"
    mkdir -p "$(dirname "$UNBOUND_CONFIG_FILE")"
    cat <<EOF > "$UNBOUND_CONFIG_FILE"
config unbound 'ub_main'
        option dns64 '0'
        option domain 'lan'
        option edns_size '1232'
        option extended_stats '0'
        option hide_binddata '1'
        option interface_auto '1'
        option listen_port '53'
        option localservice '1'
        option manual_conf '0'
        option num_threads '1'
        option protocol 'default'
        option rate_limit '0'
        option rebind_localhost '0'
        option rebind_protection '1'
        option recursion 'default'
        option resource 'default'
        option root_age '9'
        option ttl_min '120'
        option ttl_neg_max '1000'
        option unbound_control '0'
        option validator '0'
        option verbosity '1'
        list iface_wan 'wan'
        option enabled '1'
        option dhcp_link 'dnsmasq'
        list iface_trig 'lan'
        list iface_trig 'wan'

config zone 'auth_icann'
        option enabled '0'
        option fallback '1'
        option url_dir 'https://www.internic.net/domain/'
        option zone_type 'auth_zone'
        list server 'lax.xfr.dns.icann.org'
        list server 'iad.xfr.dns.icann.org'
        list zone_name '.'
        list zone_name 'arpa.'
        list zone_name 'in-addr.arpa.'
        list zone_name 'ip6.arpa.'

config zone 'fwd_isp'
        option enabled '0'
        option fallback '1'
        option resolv_conf '1'
        option zone_type 'forward_zone'
        list zone_name 'isp-bill.example.com.'
        list zone_name 'isp-mail.example.net.'

config zone 'fwd_google'
        option enabled '0'
        option fallback '1'
        option tls_index 'dns.google'
        option tls_upstream '1'
        option zone_type 'forward_zone'
        list server '8.8.4.4'
        list server '8.8.8.8'
        list server '2001:4860:4860::8844'
        list server '2001:4860:4860::8888'
        list zone_name '.'

config zone 'fwd_cloudflare'
        option enabled '0'
        option fallback '1'
        option tls_index 'cloudflare-dns.com'
        option tls_upstream '1'
        option zone_type 'forward_zone'
        list server '1.1.1.1'
        list server '1.0.0.1'
        list server '2606:4700:4700::1111'
        list server '2606:4700:4700::1001'
        list zone_name '.'
EOF
    log "已将 unbound 配置文件添加到固件中"
fi

log "固件编译完成，软件源已配置为 $MIRROR_URL，默认 Shell 已修改为 bash"
