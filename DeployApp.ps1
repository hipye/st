function AppToDevice {
    param (
        [string]$device,
        [string]$apkPath = ".\app-armeabi-v7a-debug.apk"
    )
    adb connect $device
    # Step 1: Push APK to device
    Write-Host "Pushing APK to device..."
    adb -s $device push $apkPath /data/local/tmp/
    adb -s $device push .\myUnisound2.5.1.apk /data/local/tmp/
    

    # Step 2: Install APK
    Write-Host "Installing APK..."
    adb shell settings put secure install_non_market_apps 1
    adb -s $device shell /system/bin/pm uninstall com.test.dlna
    adb -s $device shell /system/bin/pm install -r /data/local/tmp/app-armeabi-v7a-debug.apk
    adb -s $device shell /system/bin/pm install -r /data/local/tmp/myUnisound2.5.1.apk
    adb -s $device shell /system/bin/rm /data/local/tmp/myUnisound2.5.1.apk

    # Step 3: Start the main activity
    Write-Host "Starting the main activity..."
    # 首先检查当前目录是否存在 app-debug.apk
    if (Test-Path $apkPath) {
        adb -s $device shell /system/bin/am start -n com.test.dlna/.MainActivity
    } else {
        Write-Host "app-debug.apk not found in the current directory, skipping main activity start."
    }
    #adb shell /system/bin/pm grant com.test.dlna android.permission.RECEIVE_BOOT_COMPLETED
    #adb shell /system/bin/pm uninstall io.github.muntashirakon.AppManager
    #adb shell /system/bin/pm uninstall com.burakgon.dnschanger
    #adb shell /system/bin/pm uninstall com.droidlogic.mediacenter

    Write-Host "Deployment completed successfully!"
}

# Example usage:
# Deploy-AppToDevice -device "192.168.1.145:5555"

function Cadb {
    $device = "192.168.1.228"
    $port = "5555"
    # 检查端口是否为有效的数字
    if ($port -notmatch '^\d+$') {
        Write-Host "Invalid port number. Port must be a valid integer."
        return
    }
    $fullDeviceAddress = "$device`:$port"
    adb connect $fullDeviceAddress
    if ($LASTEXITCODE -eq 0) {
        Write-Host "Successfully connected to $fullDeviceAddress"
    } else {
        Write-Host "Failed to connect to $fullDeviceAddress. Please check the device's ADB debug mode, network, and firewall settings."
        # 若连接失败，提示用户输入新的端口号
        $newPort = Read-Host "Connection failed. Please enter a new port number"
        # 递归调用函数，使用新的端口号进行连接
        ConnectToDevice -port $newPort
    }
}

function push2 {
    adb -s "192.168.1.228:5555" push .\Magisk-listenapp.zip /sdcard/
.\Magisk-listenapp.zip
    adb -s "192.168.1.228:5555" shell su -c "magisk --install-module /sdcard/Magisk-listenapp.zip"
}

function Update-ModuleAndPackage {
    param(
        [Parameter(Mandatory=$true)]
        [string]$Version
    )

    # 处理 module.prop
    $modulePropPath = Join-Path -Path $PWD -ChildPath 'module.prop'
    if (-not (Test-Path $modulePropPath)) {
        Write-Error "module.prop 文件不存在于当前目录"
        return
    }

    # 读取文件内容（使用 UTF-8 无 BOM 编码）
    $content = [System.IO.File]::ReadAllText($modulePropPath, [System.Text.Encoding]::UTF8)

    # 修改版本号
    $updated = $content -replace '(?im)^(version\s*=\s*).+', "`${1}$Version"

    # 强制转换为 CR+LF 换行符
    $crlfContent = $updated -replace "`r?`n", "`r`n"

    # 确保以 UTF-8 无 BOM 编码保存文件
    $utf8NoBom = New-Object System.Text.UTF8Encoding $false
    [System.IO.File]::WriteAllText($modulePropPath, $crlfContent, $utf8NoBom)

    # 调试：验证文件编码
    Write-Host "`n==== 文件编码验证 ====" -ForegroundColor Cyan
    $fileBytes = [System.IO.File]::ReadAllBytes($modulePropPath)
    Write-Host "文件头 (HEX): $([System.BitConverter]::ToString($fileBytes[0..3]))"
    if ($fileBytes[0] -eq 0xEF -and $fileBytes[1] -eq 0xBB -and $fileBytes[2] -eq 0xBF) {
        Write-Warning "文件包含 BOM 头！"
    } else {
        Write-Host "文件无 BOM 头，符合 UTF-8 无 BOM 规范" -ForegroundColor Green
    }

    # 调试：显示文件内容
    Write-Host "`n==== 文件内容 ====" -ForegroundColor Yellow
    Get-Content $modulePropPath -Encoding UTF8 | ForEach-Object { Write-Host "| $_" }

    # ============ 打包操作 ============
    $itemsToPackage = @(
        'META-INF',
        'system',      # 现在包含移动后的 listenapp
        'create_zip.sh',
        'customize.sh',
        'custom_ex.sh',
        'magiskpolicy',
        'module.prop',
        'README',
        'service.sh',
        'uninstall.sh'
    )

    # 过滤有效文件
    $validItems = $itemsToPackage | Where-Object { Test-Path $_ }
    
    # 打包操作
    $zipPath = Join-Path -Path $PWD -ChildPath 'Magisk-listenapp.zip'
    try {
        Compress-Archive -Path $validItems -DestinationPath $zipPath -Force
        Write-Host "ZIP 包已生成: $zipPath" -ForegroundColor Green
    }
    catch {
        Write-Error "打包失败: $_"
    }
}