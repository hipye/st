if status is-interactive
    # Commands to run in interactive sessions can go here
end

function cman
man -M ../usr/share/man/zh_CN $argv
end

function treepost
tree $HOME/blog/content/posts
end

function upbdime
    set Usepwd $PWD
    cd /mnt/sdcard/baidu/ime && rm chiku*.zip
    7z a chiku.zip ch3.txt en2.txt && sudo mv chiku.zip chiku(date +"%Y-%m-%d-%H-%M-%S").zip
    aliyunpan u chiku.zip /baiduchiku
    echo -e '\n词库备份成功' && cd
end

function vimfish
    set Usepwd $PWD
    cd $HOME/st;vim config.fish
    cp config.fish $HOME/.config/fish;echo -e '\n复制成功'
    source $HOME/.config/fish/config.fish
    cd $Usepwd
end

function proxy
    set -xg ALL_PROXY socks5://192.168.8.1:10811
    git config --global http.proxy "socks5://192.168.8.1:10811"
    git config --global https.proxy "socks5://192.168.8.1:10811"
    export GO111MODULE=on
    export GOPROXY=https://goproxy.io
    curl ipba.cc
end

function unproxy
    set -e ALL_PROXY
    git config --global --unset http.proxy
    git config --global --unset https.proxy
    curl ipba.cc
end

function gp
    set Usepwd $PWD
    read commit_git && git add . && git commit -m "$commit_git" && cd $Usepwd && echo -e '\ngood luck'
end

function gpp
    set Usepwd $PWD
    git pull && read commit_git && git add . && git commit -m "$commit_git" && git push && cd $Usepwd && echo -e '\ngood luck'
end

function vimbashrc
    vim $HOME/st/.bashrc && cp .bashrc $HOME/
end

function md
    cd $HOME && mkdir -p merge
    read blogname
        if [ ! -f /mnt/sdcard/Documents/PureWriter/未命名.md ]
            echo -e "\n不存在该文件"
        else
            cd blog && hugo new content/posts/"$blogname".md
            echo lastmod: (date +"%Y-%m-%dT%H:%M:%S")Z |tee timetime
            sed -i '/lastmod: /d' content/posts/"$blogname".md
            sed -i '3 r timetime' content/posts/"$blogname".md && echo -e "添加修改日期"
            sed -i  '/draft: .*/,+1000000d' content/posts/"$blogname".md
            sed 's/license: .*/license: ""\ndraft: false\n---/g' content/posts/"$blogname".md |tee content/posts/"$blogname".md.new
            echo -e "\n内容重置成功" && rm timetime
            rm content/posts/"$blogname".md && mv content/posts/"$blogname".md.new content/posts/"$blogname".md
            echo -e "\n旧内容删除" && cd $HOME
            mv /mnt/sdcard/Documents/PureWriter/未命名.md $HOME/merge/
            mv blog/content/posts/"$blogname".md $HOME/merge/
            cd merge && cat 未命名.md >> $blogname.md
            mv $blogname.md $HOME/blog/content/posts/
            cd && rm -rf $HOME/merge
            echo -e "\n替换成功"
            cd blog && hugo
        end
end
