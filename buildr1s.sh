#!/bin/bash

# 设置变量
OPENWRT_DIR="$HOME/lede"  # OpenWrt 源码目录
APP_NAME="luci-app-r1-server"  # 应用名称
GO_SRC="r1-server.go"  # Go 程序源码文件
GO_BIN="r1-server"  # Go 程序二进制文件名
PKG_DIR="$OPENWRT_DIR/package/feeds/luci/applications/$APP_NAME"  # 应用目录

# 检查 OpenWrt 源码目录是否存在
if [ ! -d "$OPENWRT_DIR" ]; then
    echo "OpenWrt 源码目录不存在: $OPENWRT_DIR"
    exit 1
fi

# 检查 Go 程序源码文件是否存在
if [ ! -f "$GO_SRC" ]; then
    echo "Go 程序源码文件不存在: $GO_SRC"
    exit 1
fi

# 创建应用目录
mkdir -p "$PKG_DIR/luasrc/controller" "$PKG_DIR/luasrc/view" "$PKG_DIR/root/usr/bin"

# 交叉编译 Go 程序
echo "交叉编译 Go 程序..."
GOOS=linux GOARCH=arm GOARM=7 go build -o "$PKG_DIR/root/usr/bin/$GO_BIN" "$GO_SRC"
if [ $? -ne 0 ]; then
    echo "Go 程序编译失败"
    exit 1
fi

# 创建 Shell 脚本
echo "创建 Shell 脚本..."
cat > "$PKG_DIR/root/usr/bin/r1-server-control.sh" <<EOF
#!/bin/sh

case "\$1" in
    start)
        /usr/bin/$GO_BIN &
        echo "R1 Server started on 0.0.0.0:19999"
        ;;
    stop)
        killall $GO_BIN
        echo "R1 Server stopped"
        ;;
    *)
        echo "Usage: \$0 {start|stop}"
        exit 1
        ;;
esac

exit 0
EOF
chmod +x "$PKG_DIR/root/usr/bin/r1-server-control.sh"

# 创建 LuCI 控制器
echo "创建 LuCI 控制器..."
cat > "$PKG_DIR/luasrc/controller/r1-server.lua" <<EOF
module("luci.controller.r1-server", package.seeall)

function index()
    entry({"admin", "services", "r1-server"}, firstchild(), "R1 Server", 60).dependent = false
    entry({"admin", "services", "r1-server", "control"}, call("action_control"), "Control", 10)
end

function action_control()
    local http = require "luci.http"
    local sys = require "luci.sys"

    if http.formvalue("start") then
        sys.call("/usr/bin/r1-server-control.sh start")
        http.redirect(luci.dispatcher.build_url("admin/services/r1-server/control"))
        return
    end

    if http.formvalue("stop") then
        sys.call("/usr/bin/r1-server-control.sh stop")
        http.redirect(luci.dispatcher.build_url("admin/services/r1-server/control"))
        return
    end

    luci.template.render("r1-server/control")
end
EOF

# 创建 LuCI 视图
echo "创建 LuCI 视图..."
cat > "$PKG_DIR/luasrc/view/r1-server.htm" <<EOF
<%+header%>
<h2><%:R1 Server Control%></h2>
<div class="cbi-section">
    <form method="post" action="<%=luci.dispatcher.build_url("admin/services/r1-server/control")%>">
        <div class="cbi-section-node">
            <input type="submit" name="start" value="<%:Start Server%>" class="cbi-button cbi-button-apply" />
            <input type="submit" name="stop" value="<%:Stop Server%>" class="cbi-button cbi-button-reset" />
        </div>
    </form>
</div>
<%+footer%>
EOF

# 创建 Makefile
echo "创建 Makefile..."
cat > "$PKG_DIR/Makefile" <<EOF
include \$(TOPDIR)/rules.mk

PKG_NAME:=$APP_NAME
PKG_VERSION:=1.0
PKG_RELEASE:=1

include \$(INCLUDE_DIR)/package.mk

define Package/luci-app-r1-server
  SECTION:=luci
  CATEGORY:=LuCI
  SUBMENU:=Applications
  TITLE:=R1 Server Control
  PKGARCH:=all
endef

define Package/luci-app-r1-server/description
  A LuCI application to control the R1 Server.
endef

define Build/Prepare
    mkdir -p \$(PKG_BUILD_DIR)
    \$(CP) ./luasrc \$(PKG_BUILD_DIR)/
    \$(CP) ./root \$(PKG_BUILD_DIR)/
endef

define Package/luci-app-r1-server/install
    \$(INSTALL_DIR) \$(1)/usr/lib/lua/luci/controller
    \$(INSTALL_DATA) \$(PKG_BUILD_DIR)/luasrc/controller/r1-server.lua \$(1)/usr/lib/lua/luci/controller/

    \$(INSTALL_DIR) \$(1)/usr/lib/lua/luci/view
    \$(INSTALL_DATA) \$(PKG_BUILD_DIR)/luasrc/view/r1-server.htm \$(1)/usr/lib/lua/luci/view/

    \$(INSTALL_DIR) \$(1)/usr/bin
    \$(INSTALL_BIN) \$(PKG_BUILD_DIR)/root/usr/bin/$GO_BIN \$(1)/usr/bin/
    \$(INSTALL_BIN) \$(PKG_BUILD_DIR)/root/usr/bin/r1-server-control.sh \$(1)/usr/bin/
endef

\$(eval \$(call BuildPackage,luci-app-r1-server))
EOF

# 编译 OpenWrt 应用
echo "编译 OpenWrt 应用..."
cd "$OPENWRT_DIR"
make package/feeds/luci/applications/$APP_NAME/compile V=s

if [ $? -eq 0 ]; then
    echo "应用编译成功！"
    echo "生成的 IPK 文件在: $OPENWRT_DIR/bin/packages/*/luci/luci-app-r1-server_*.ipk"
else
    echo "应用编译失败"
    exit 1
fi
