#!/bin/bash
function Server() {
https://i.u103357.nyat.app:29999
https://www.u103357.nyat.app:18081
ld.frp.one:52001/webdav
root@ld.frp.one -p 52222
root@frphn1.chickfrp.com -p 52222
frphn1.chickfrp.com:19999/bind
frpnn1.chickfrp.com:53000/help
frpnn1.chickfrp.com:52001/webdav
156.246.94.56:10810
47.115.226.60:19999
101.132.138.160:10811
110.42.59.65:52225/cgi-bin/luci/
}
export LANG=en_US.UTF-8
stty erase ^?
stty kill '^U'

R='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
RESET='\033[0m' # 恢复默认颜色

function color_echo() {
  local color=$1
  shift
  echo -e "${color}$@${RESET}"
}

export USERNAME="morizen@163.com" 
export PASSWORD="axd4cfqh8n2dmjk9"
# 检查 ZipPass 变量是否存在
if [[ -n "${ZipPass}" ]]; then
	password="${ZipPass}"
	echo "使用系统变量 ZipPass 作为密码。"
else
	# 如果 ZipPass 不存在，提示用户输入密码
	read -p "请输入全局解密密码：" password
fi
  # 使用密码进行后续操作
  #	echo "密码已设置为：$password"
#read -p "请输入全局解密密码： " password

function Tmx_pkg() {
	pkg upgrade -y 
	pkg install vim p7zip curl wget git
}
function Server() {
	sudo apt intsall nodejs npm
	npm install express express-fileupload
}

function Ubt_apt() {
	sudo locale-gen zh_CN.UTF-8
	sudo update-locale LANG=zh_CN.UTF-8
	sudo apt-get update -y
	sudo apt-get upgrade -y
	sudo apt-get install -y vim node curl wget p7zip-rar p7zip p7zip-full p7zip-full p7zip-full p7zip-rar
}

function Script() {
	cd
if [ -e "$HOME/.moriz/syncScript.sh" ]; then
	color_echo $R "脚本存在。"
else
	mkdir $HOME/.moriz
	wget https://raw.giteeusercontent.com/hipye/st/raw/master/syncScript.sh
	source $HOME/.moriz/syncScript.sh
fi
}

function bashrc() {
	cd $HOME
	local Ex="$1"
if [ -e "$HOME/.ssh/${Ex}.7z" ]; then
	rm $Ex.7z
	curl -O https://raw.giteeusercontent.com/hipye/st/raw/master/$Ex.7z
	rm $HOME/.bashrc
	7z x -p"${password}" $Ex.7z && rm $Ex.7z
	color_echo $R ".bashrc 已经导入。"
	rm $HOME/${Ex}.7z*
	source .bashrc
else
	curl -O https://raw.giteeusercontent.com/hipye/st/raw/master/$Ex.7z
	rm $HOME/.bashrc
	7z x -p"${password}" $Ex.7z && rm $Ex.7z
	color_echo $R ".bashrc 已经导入。"
	rm $HOME/${Ex}.7z*
	source .bashrc
fi
}

function Hugo() {
	cd $HOME
if command -v hugo > /dev/null 2>&1; then
	color_echo $R "hugo 已经安装。"
else
	if [ -e "$HOME/hugo_0.114.0_Termux_aarch64.deb" ]; then
	dpkg -i ./hugo_0.114.0_Termux_aarch64.deb
	else
	curl -O https://raw.giteeusercontent.com/hipye/st/raw/master/hugo_0.114.0_Termux_aarch64.deb
	dpkg -i ./hugo_0.114.0_Termux_aarch64.deb
	fi
fi	
}

function Aria2() {
	cd $HOME
if command -v aria2c > /dev/null 2>&1; then
	color_echo $R "(: aria2 已经安装。"
else
	if [[ "$OSTYPE" == "linux-gnu" ]] && [[ "$arch" = "x86_64" ]]; then
	curl -O https://raw.giteeusercontent.com/hipye/st/raw/master/aria2-x86_64-U.deb
	sudo apt-get install ./aria2-x86_64-U.deb -y
	rm aria2-x86_64-x86_U.deb
	elif [[ "$OSTYPE" == "linux-gnu" ]] && [[ "$arch" = "aarch64" ]]; then
	 curl -O https://raw.giteeusercontent.com/hipye/st/raw/master/aria2-aarch64-U-4.deb
	 sudo apt-get install ./aria2-aarch64-U-4.deb -y
	 rm aria2-aarch64-U-4.deb
	fi
fi
}

function SSH() {
if [ -e "$HOME/.ssh/authorized_keys" ]; then
	color_echo $R "(: authorized_keys 已经存在。"
else
	mkdir -p $HOME/.ssh
	cd && rm JOIN.7z
	curl -O https://raw.giteeusercontent.com/hipye/st/raw/master/JOIN.7z
	sleep 3
	color_echo $R "删除旧有文件下载新文件。"
    #mv JOIN.7z .ssh/
	#cd .ssh/
	echo -e "\n导入SSH\n"
	7z x -p"${password}" JOIN.7z && rm JOIN.7z
    #mv .ssh/* ./ && rm -rf .ssh/
fi
}

function Git() {
r_echo() {
    local color="$1"
    local message="$2"
    echo -e "${color}${message}\033[0m"
}
	cd $HOME
	curl -O https://gitee.com/hipye/st/raw/master/.gitconfig
	color_echo $R "(: gitconfig 已经导入。"
	git clone git@gitee.com:hipye/st.git
}


install_aliyunpan() {
    local download_url="$1"
    local is_termux="$2"
    local HOME_DIR="$HOME"
    local zip_file="${download_url##*/}"
    local extracted_dir="aliyunpan"
    local actual_extracted_dir=$(basename "$download_url" .zip)

    cd "$HOME_DIR" || {
        color_echo "\033[0;31m" "无法进入 $HOME_DIR 目录。"
        return 1
    }

    if [ ! -f "$zip_file" ]; then
        echo "开始下载 $download_url"
        wget "$download_url" || {
            color_echo "\033[0;31m" "下载 $download_url 失败。"
            return 1
        }
    fi

    if [ -f "$zip_file" ]; then
        echo "开始解压 $zip_file"
        7z x "$zip_file" || {
            color_echo "\033[0;31m" "解压 $zip_file 失败。"
            return 1
        }
    else
        color_echo "\033[0;31m" "压缩包文件 $zip_file 不存在，无法解压。"
        return 1
    fi

    # 输出解压后的目录内容，用于调试
    echo "解压后的目录内容:"
    ls -l

    if [ -d "$actual_extracted_dir" ]; then
        echo "重命名目录"
        mv "$actual_extracted_dir" "$extracted_dir"
    fi

    if [ ! -d "$extracted_dir" ]; then
        color_echo "\033[0;31m" "未找到 $extracted_dir 目录。"
        return 1
    fi

    chmod 755 "$extracted_dir"

    if [ "$is_termux" = "true" ]; then
        local INSTALL_DIR="/data/data/com.termux/files/usr"
        local BINARY_DIR="$INSTALL_DIR/bin"
        mv "$extracted_dir" "$INSTALL_DIR" || {
            color_echo "\033[0;31m" "移动到安装目录失败。"
            return 1
        }
        ln -s "$INSTALL_DIR/$extracted_dir/$extracted_dir" "$BINARY_DIR/$extracted_dir" || {
            color_echo "\033[0;31m" "创建符号链接失败。"
            return 1
        }
    else
        local INSTALL_DIR="/usr/local/aliyunpan"
        local BINARY_DIR="/usr/local/bin"
        sudo mkdir -p "$INSTALL_DIR"
        sudo mv "$extracted_dir" "$INSTALL_DIR" || {
            color_echo "\033[0;31m" "移动到安装目录失败。"
            return 1
        }
        sudo ln -s "$INSTALL_DIR/$extracted_dir/$extracted_dir" "$BINARY_DIR/$extracted_dir" || {
            color_echo "\033[0;31m" "创建符号链接失败。"
            return 1
        }
    fi

    echo "清理下载文件"
    rm -rf "$zip_file"

    if command -v aliyunpan > /dev/null 2>&1; then
        aliyunpan config set --max_download_parallel 8
        aliyunpan config set --max_upload_parallel 8
        aliyunpan config set --cache_size 64KB
        aliyunpan config set --savedir $HOME/
    else
        color_echo "\033[0;31m" "aliyunpan 未安装，无法执行配置命令。"
    fi

    color_echo "\033[0;32m" "$extracted_dir 安装成功。"
}

# 整合后的 Aliyunpan 函数
function Aliyunpan() {
    local APP_NAME="aliyunpan"
    local R='\033[0;31m'

    if command -v "$APP_NAME" > /dev/null 2>&1; then
        color_echo $R "$APP_NAME 已经安装。"
        return
    fi

    local arch=$(uname -m)
    local is_termux=false
    if [ -d "/data/data/com.termux/files/usr" ]; then
        is_termux=true
    fi

    case $arch in
        x86_64)
            if [ "$is_termux" = "true" ]; then
                color_echo $R "Termux 环境不支持 x86_64 架构。"
                return 1
            fi
            local download_url="https://github.com/tickstep/aliyunpan/releases/download/v0.3.3/aliyunpan-v0.3.3-linux-amd64.zip"
            echo "即将使用的下载链接: $download_url"
            install_aliyunpan "$download_url" "$is_termux"
            ;;
        aarch64)
            if [ "$is_termux" = "true" ]; then
                local download_url="https://github.com/tickstep/aliyunpan/releases/download/v0.3.3/aliyunpan-v0.3.3-android-api21-arm64.zip"
                echo "即将使用的下载链接: $download_url"
                install_aliyunpan "$download_url" "$is_termux"
            else
                local download_url="https://github.com/tickstep/aliyunpan/releases/download/v0.3.3/aliyunpan-v0.3.3-linux-arm64.zip"
                echo "即将使用的下载链接: $download_url"
                install_aliyunpan "$download_url" "$is_termux"
            fi
            ;;
        *)
            color_echo $R "不支持的系统架构: $arch"
            return 1
            ;;
    esac
}

function minidlna() {
	cd $HOME
	sudo apt-get install minidlna
	# 安装minidlna
	sudo service minidlna start
	# 启动minidlna
	sudo vim /etc/minidlna.conf
	# 修改默认配置
}

function BBDown() {
	wget https://github.com/nilaoda/BBDown/releases/download/1.6.3/BBDown_1.6.3_20240814_linux-arm64.zip
	unzip BBDown*.zip
}

function Vim_config() {
	cd $HOME
if [ -e "$HOME/.config/vim/.vimrc" ]; then
	color_echo $R "(: .vimrc 已经存在。"
	sleep 0.1
else
	mkdir -p $HOME/.config/vim/
	curl -O https://raw.giteeusercontent.com/hipye/st/raw/master/.vimrc_custom
	mv .vimrc_custom .vimrc
fi
}
function 7z() {
set -e  # 任何命令失败时立即退出

# 创建目录并下载源码
mkdir -p p7zip
wget https://www.7-zip.org/a/7z2409-src.tar.xz -O 7z2409-src.tar.xz

# 解压源码（正确使用-J选项处理.xz文件）
tar -xJvf 7z2409-src.tar.xz -C p7zip

# 编译各个组件
cd p7zip/CPP/7zip/Bundles/Alone2 && make -j$(nproc) -f makefile.gcc
cd ../Alone && make -j$(nproc) -f makefile.gcc
cd ../Alone7z && make -j$(nproc) -f makefile.gcc
cd ../Format7zF && make -j$(nproc) -f makefile.gcc

# 安装所有二进制文件
sudo install -m 755 Alone2/_o/7zz /usr/local/bin/
sudo install -m 755 Alone/_o/7za /usr/local/bin/
sudo install -m 755 Alone7z/_o/7zr /usr/local/bin/

echo "安装完成，可执行文件已复制到 /usr/local/bin"
}

function JQ() {
cd /mingw64/bin
curl -o jq.exe https://gitlab.com/hipye/apps/-/raw/master/jq-windows-amd64.exe?ref_type=heads
}

# 环境检测与配置
if [[ "$OSTYPE" == *"msys"* || "$OSTYPE" == *"cygwin"* ]]; then
    # 如果是 Git Bash 环境
    color_echo $R "当前为 Git Bash 环境"
    bashrc "BASHRC_G"
    Vim_config
    SSH
    Git
elif command -v apt >/dev/null 2>&1; then
    os_name=$(grep '^NAME=' /etc/os-release | cut -d= -f2 | tr -d '"')
    arch=$(uname -m)
    if [[ "$os_name" == "Ubuntu" ]]; then
        if [[ "$arch" == "x86_64" ]]; then
            color_echo $R "检测到 Ubuntu x86_64 环境"
           #Ubt_apt
           #bashrc "BASHRC"
           #Git
           #Vim_config
           #SSH
            Aliyunpan
        elif [[ "$arch" == "aarch64" ]]; then
            color_echo $R "检测到 Ubuntu aarch64 环境"
            Ubt_apt
            bashrc "BASHRC_A64"
            Aliyunpan_aarch64
            Vim_config
            SSH
            Git
            #minidlna
        else
            color_echo $R "不支持的架构: $arch"
            exit 1
        fi
    else
        color_echo $R "不支持的 Linux 发行版: $os_name"
        exit 1
    fi
elif [ -f "$HOME/.termux/termux.properties" ]; then
    color_echo $R "检测到 Termux 环境"
    Tmx_pkg
    bashrc "TBASHRC"
    Git
    Vim_config
    SSH
    Aliyunpan
    Script
    Hugo
else
    color_echo "31" "未知环境，退出脚本"
    exit 1
fi
