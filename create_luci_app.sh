#!/bin/bash

# 目标目录
TARGET_DIR="$HOME/lede/package/lean"
APP_NAME="r1Config"
APP_DIR="${TARGET_DIR}/luci-app-${APP_NAME}"

# 检查目标目录是否存在
if [ ! -d "${TARGET_DIR}" ]; then
    echo "错误：目标目录 ${TARGET_DIR} 不存在！"
    exit 1
fi

# 创建目录结构
mkdir -p ${APP_DIR}/files/usr/lib/lua/luci/controller
mkdir -p ${APP_DIR}/files/usr/lib/lua/luci/model/cbi/${APP_NAME}
mkdir -p ${APP_DIR}/files/usr/lib/lua/luci/view/${APP_NAME}

# 创建控制器文件
cat > ${APP_DIR}/files/usr/lib/lua/luci/controller/${APP_NAME}.lua <<EOF
module("luci.controller.${APP_NAME}", package.seeall)

function index()
    entry({"admin", "services", "${APP_NAME}"}, cbi("${APP_NAME}/config"), _("${APP_NAME}"), 60)
end
EOF

# 创建 CBI 模型文件
cat > ${APP_DIR}/files/usr/lib/lua/luci/model/cbi/${APP_NAME}/config.lua <<EOF
local uci = luci.model.uci.cursor()

m = Map("${APP_NAME}", translate("${APP_NAME} Settings"), translate("Configure ${APP_NAME} settings here."))

s = m:section(TypedSection, "config", translate("General Settings"))
s.anonymous = true

s:option(Value, "hostname", translate("Hostname"))
s:option(Value, "ip_address", translate("IP Address"))
s:option(Value, "netmask", translate("Netmask"))
s:option(Value, "gateway", translate("Gateway"))

return m
EOF

# 创建视图文件
cat > ${APP_DIR}/files/usr/lib/lua/luci/view/${APP_NAME}/config.htm <<EOF
<%+header%>
<h2><%:${APP_NAME} Settings%></h2>
<div class="cbi-map">
    <div class="cbi-map-descr"><%:Configure ${APP_NAME} settings here.%></div>
    <div class="cbi-section">
        <%+cbi/ucisection%>
    </div>
</div>
<%+footer%>
EOF

# 创建 Makefile
cat > ${APP_DIR}/Makefile <<EOF
include \$(TOPDIR)/rules.mk

LUCI_TITLE:=${APP_NAME} - Configuration Tool
LUCI_DEPENDS:=

include \$(TOPDIR)/feeds/luci/luci.mk

# call BuildPackage - OpenWrt buildroot signature
EOF

# 输出成功信息
echo "Luci 应用 ${APP_DIR} 已创建成功！"
echo "目录结构："
tree ${APP_DIR}

echo "下一步："
echo "1. 进入 OpenWrt 源码目录："
echo "   cd $HOME/lede"
echo "2. 运行以下命令编译和安装："
echo "   make package/lean/luci-app-${APP_NAME}/compile V=s"
echo "   make package/lean/luci-app-${APP_NAME}/install V=s"
echo "3. 登录 Luci 界面，查看 Services 菜单下的 ${APP_NAME}。"
